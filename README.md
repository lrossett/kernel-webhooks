# Kernel Webhooks

This repository contains the code for kernel webhooks for various Red Hat
gitlab projects.

These webhooks interact with the kernel projects and apply labels, run CI
jobs, perform BZ checks, among other things.

## Hacking

Clone the repository, and install all Python dependencies with

```bash
pip3 install --user --force-reinstall -e .
```

If you run into any problems that seem to suggest an out-of-date or missing
dependency, rerun the command above.

Please run

```bash
tox
```

before submitting any changes.

## Running a webhook for one merge request

Run the webhook manually on a merge request URL, optionally increasing the
logging level to `DEBUG` via something like

```shell
CKI_LOGGING_LEVEL=DEBUG \
    python3 -m webhook.public \
    --merge-request https://gitlab.com/group/repo/-/merge_requests/1
```

To allow write operations, enable production support, expose the GitLab private
tokens in your environment and define an environment variable `GITLAB_TOKENS` like

```shell
export IS_PRODUCTION='true'
export COM_GITLAB_TOKEN='1234567890abcedf'
export GITLAB_TOKENS='{"gitlab.com": "COM_GITLAB_TOKEN"}'
```

You will also need to specify/export the gitlab token as shown above in the
case you are dealing with a private project inside gitlab, otherwise you will
get a cryptic "404 not found" errors when running the webhook.

## Running a webhook for processing bugzilla validation

You can trigger bugzilla validation or re-validation by manually running:

```shell
CKI_LOGGING_LEVEL=DEBUG \
    python3 -m webhook.bugzilla \
    --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
    --note request-bz-evaluation
```

The --note parameter "fakes" a comment being added to a merge request, as if
you added it through the gitlab webui yourself (but the comment itself isn't
actually added there). The example above shows the predefined comment keyword we
have to trigger bugzilla re-evaluation.

## Design

Webhooks were designed to have a quick response time.  Due to the complexities
of what the webhooks need to do and the unstable nature of network
infrastructure, a message queue was introduced to aid in robustness.

A RabbitMQ server quickly receives the hook and broadcasts it to all the
listeners.  Any infrastructure issues fails to send the message ack,
therefore the message is resent.

In addition, some webhook data needs access to the internal Red Hat network.
The RabbitMQ allows encrypted communication for consumers inside the
firewall to access the message and respond appropriately to the public
gitlab server.

```
 +-------+      +----------+      +----------+
 |Gitlab |      | Webhook  |      | RabbitMQ |
 |Project|----->| receiver |----->|          |
 +-------+      +----------+      +----------+
                                       |
                   Public internet     |
                   --------------------|------------
                   Internal network    |
                                       |
                               +-------+-------+
                               |               |
                               v               v
                           +--------+     +---------+
                           | Public |     |Bugzilla |
                           | Queue  |     |Queue    |
                           +--------+     +---------+
```

Everything is managed in containers using OpenShift. The webhook receiver and
the RabbitMQ server run in CentOS CI. The containers handling the actual
webhook processing run inside the RH intranet. The service is managed by the
CKI team.

## Local setup

All the services have a `run_` script associated with them.  One can run each
script manually to simulate the service and then use curl to feed in data.

Or one can connect to the public RabbitMQ server using the run-time generated
unique queue to listen in. A password will be required.

Connecting to the RabbitMQ server:

```bash
export RABBITMQ_HOST="public-rabbitmq-ark.apps.ocp.ci.centos.org"
export RABBITMQ_USER=kernel
export RABBITMQ_PASSWORD=<PASSWORD>
export RABBITMQ_PORT="443"

./run_public.sh
<goto gitlab project->settings->webhooks->Test>
```

Console logs for the containers can be found at
<https://console-openshift-console.apps.ocp4.prod.psi.redhat.com/k8s/ns/cki/pods>.

## Deployment

This repository is setup to rebuild the containers after each merge and push them
to the registry.

These containers listen on a RabbitMQ message bus and do not filter on
wildcards for security reasons.  Instead they filter on requested events
based on `<location>.<group>.<project>.<event>`.  For example,

```
gitlab.com.cki-project.kernel-ark.merge_request
```

When adding new webhooks or events, please consult
<https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/secrets.yml>.

and verify `KERNEL_WEBHOOKS_<container>_ROUTING_KEYS` matches those events.
If not, add an entry per event and submit an MR for that tree.


### external_ci webhook specifics

Webhook for external CI on GitLab merge requests.

#### Environment variables

On top of common expected variables, the following ones are required:

* `EXTERNAL_CI_ROUTING_KEYS`: Whitespace-separated list of RabbitMQ routing keys.
* `EXTERNAL_CI_QUEUE`: RabbitMQ queue name.
* `CONFIG_PATH`: Path to the project configuration to use.
* Trigger tokens for trusted and external pipelines. Variable names are configurable
  under `.external_trigger_token_name` and `.trusted_trigger_token_name` settings
  in the config file.

#### Configuration format and example

Multiple projects per single webhook instance are supported.

Example of two project configurations sharing the pipeline projects:

```
.pipeline_configs:
  .cki_trusted_pipeline_project: <path_with_namespace>
  .cki_trusted_pipeline_branch: <branch_name>
  .trusted_trigger_token_name: <variable_name>
  .cki_external_pipeline_project: <path_with_namespace>
  .cki_external_pipeline_branch: <branch_name>
  .external_trigger_token_name: <variable_name>

project_name:
  .extends: .pipeline_configs
  .mr_project: <path_with_namespace>
  .members_of: <group or project_path_with_namespace>

project_name2:
  .extends: .pipeline_configs
  .mr_project: <path_with_namespace>
  .members_of: <group or project_path_with_namespace>
```

Example of two project configurations *not* sharing the pipeline projects:

```
project_name:
  .cki_trusted_pipeline_project: <path_with_namespace>
  .cki_trusted_pipeline_branch: <branch_name>
  .trusted_trigger_token_name: <variable_name>
  .cki_external_pipeline_project: <path_with_namespace>
  .cki_external_pipeline_branch: <branch_name>
  .external_trigger_token_name: <variable_name>
  .mr_project: <path_with_namespace>
  .members_of: <group or project_path_with_namespace>

project_name2:
  .cki_trusted_pipeline_project: <path_with_namespace>
  .cki_trusted_pipeline_branch: <branch_name>
  .trusted_trigger_token_name: <variable_name>
  .cki_external_pipeline_project: <path_with_namespace>
  .cki_external_pipeline_branch: <branch_name>
  .external_trigger_token_name: <variable_name>
  .mr_project: <path_with_namespace>
  .members_of: <group or project_path_with_namespace>
```

Add any variables required by the pipelines into either the project-specific or
common settings depending on your needs.

You can also specify only one of the external/trusted-but-blocked pipelines and
leave out the other if that's what better fits your needs.
