"""Tests for external CI module."""
import unittest
from unittest import mock

import webhook.external_ci as external_ci

from tests import fakes


def init_fake_gitlab():
    """Initialize and return a fake gitlab.Gitlab instance."""
    glab = fakes.FakeGitLab()
    glab.add_group('group')
    glab.add_project('path/to/project')
    add_members_to = [glab.groups.get('group'),
                      glab.projects.get('path/to/project')]
    for g_or_p in add_members_to:
        g_or_p.members.add_member('testmember')
        g_or_p.members.add_member('anothermember')

    return glab


class TestProcessMessage(unittest.TestCase):
    """Tests for external_ci.process_message()."""

    @mock.patch('cki_lib.messagequeue.Message.gl_instance', mock.Mock())
    @mock.patch('gitlab.Gitlab.auth', mock.Mock())
    def test_unrecognized_data(self):
        """Verify unrecognized messages are discarded."""
        message = {}
        with self.assertLogs('cki.webhook.external_ci', level='ERROR') as logs:
            external_ci.process_message('key', message)
            self.assertIn('Missing object_kind in message', logs.output[-1])

        message = {'object_kind': 'unknown'}
        with self.assertLogs('cki.webhook.external_ci',
                             level='WARNING') as logs:
            external_ci.process_message('key', message)
            self.assertIn('Unknown hook type: unknown', logs.output[-1])

    @mock.patch('webhook.external_ci.update_pipeline_data')
    @mock.patch('cki_lib.messagequeue.Message.gl_instance')
    @mock.patch('gitlab.Gitlab.auth', mock.Mock())
    def test_pipeline_call(self, mock_gl, mock_update):
        """Verify correct handler is called for pipeline hook."""
        message = {'object_kind': 'pipeline'}
        external_ci.process_message('key', message)
        mock_update.assert_called_with(mock_gl.return_value, message)

    @mock.patch('cki_lib.messagequeue.Message.gl_instance', mock.Mock())
    @mock.patch('gitlab.Gitlab.auth', mock.Mock())
    def test_mr_hook_no_project(self):
        """Verify MR with unknown project is handled correctly."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'unknown'}}

        with self.assertLogs('cki.webhook.external_ci', level='ERROR') as logs:
            external_ci.process_message('key', message)
            self.assertIn('Missing config for', logs.output[-1])

    @mock.patch('webhook.external_ci.get_project_config')
    @mock.patch('cki_lib.messagequeue.Message.gl_instance')
    def test_mr_hook_group_member(self, mock_gl, mock_config):
        """Check pipelines aren't triggered if the author is a group member."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'projectpath'},
                   'user': {'username': 'testmember'}}
        mock_config.return_value = {'.members_of': 'group'}
        mock_gl.return_value = init_fake_gitlab()

        with self.assertLogs('cki.webhook.external_ci', level='DEBUG') as logs:
            external_ci.process_message('key', message)
            self.assertIn('Found internal contributor', logs.output[-1])

    @mock.patch('webhook.external_ci.get_project_config')
    @mock.patch('cki_lib.messagequeue.Message.gl_instance')
    def test_mr_hook_project_member(self, mock_gl, mock_config):
        """Test fallback with testing for a project member instead of group."""
        message = {'object_kind': 'merge_request',
                   'project': {'path_with_namespace': 'projectpath'},
                   'user': {'username': 'anothermember'}}
        mock_config.return_value = {'.members_of': 'path/to/project'}
        mock_gl.return_value = init_fake_gitlab()

        with self.assertLogs('cki.webhook.external_ci', level='DEBUG') as logs:
            external_ci.process_message('key', message)
            self.assertIn('Found internal contributor', logs.output[-1])


class TestMain(unittest.TestCase):
    """Sanity tests for external_ci.main()."""

    @mock.patch.dict('os.environ', {'CONFIG_PATH': 'filepath'})
    @mock.patch.dict('webhook.external_ci.CI_CONFIG',
                     {'test': {'.mr_project': 'path/to/project',
                               '.members_of': 'path/to/project'}})
    @mock.patch('webhook.external_ci.load_config', mock.Mock())
    @mock.patch('webhook.common.get_instance')
    def test_manual(self, mock_gitlab):
        """Verify manual request is correctly handled.

        This includes checking get_manual_hook_data() function.
        """
        args = ['--merge-request',
                'https://gitlab.example/path/to/project/-/merge_requests/11']
        fake_gitlab = init_fake_gitlab()
        fake_project = fake_gitlab.projects.get('path/to/project')
        fake_project.attributes = {
            'name': 'path/to/project',
            'http_url_to_repo': 'https://gitlab.example/path/to/project'
        }
        fake_project.add_mr(
            11,
            {'source_project_id': 'path/to/project',
             'sha': 'last_commit',
             'author': {'username': 'testmember'},
             'target_branch': 'devel',
             'source_branch': 'mybranch'}
        )
        mock_gitlab.return_value = fake_gitlab

        with self.assertLogs('cki.webhook.external_ci', level='DEBUG') as logs:
            external_ci.main(args)
            self.assertIn('Found internal contributor', logs.output[-1])
