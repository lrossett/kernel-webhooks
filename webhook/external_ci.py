"""Event handlers."""
import copy
import os
import sys

import cki_lib.misc
import gitlab
import pipeline_tools.utils as utils
from cki_lib.logger import get_logger
from cki_lib.messagequeue import Message
from yaml import load
from yaml import Loader

from . import common
from . import settings


LOGGER = get_logger('cki.webhook.external_ci')
CI_CONFIG = {}

EXTERNAL_LABEL_NAME = 'External Contribution'
EXTERNAL_LABEL_DESCRIPTION = 'MR submitted by an external contributor'
EXTERNAL_LABEL_COLOR = '#FF0000'


EMOJI_MAP = {
    'created': ':hourglass_flowing_sand:',
    'pending': ':hourglass_flowing_sand:',
    'running': ':hourglass_flowing_sand:',
    'canceled': ':grey_exclamation:',
    'skipped': ':grey_exclamation:',
    'success': ':heavy_check_mark:',
    'failed': ':sob:',
    'manual': ':no_entry:'
}


WELCOME_MESSAGE = """Hi! This is the friendly CKI test bot.

It appears that you are not a member of {group}. This means that the CI
pipeline on your MR will fail. As getting testing is important, I'll be
responsible for testing your changes. After every MR change, I'll start a
small testing pipeline and link it here so you can follow the results. I'll
also create and link a pipeline for hardware testing that the reviewers can
start to get extra test coverage."""


PIPELINE_MESSAGE = """ testing pipeline:

&nbsp;&nbsp;{link} - {status} {emoji}


"""


def already_commented(merge_request, bot_name):
    """Check if bot_name user already added a welcome comment on the MR.

    Args:
        merge_request: MR object as returned by the gitlab module
        bot_name: Username of the bot account to check.

    Returns:
        True if a first comment of a discussion was added by the bot
        False if there is no first discussion comment added by the bot
    """
    return any(d.attributes['notes'][0]['author']['username'] == bot_name
               for d in merge_request.discussions.list(as_list=False))


def ensure_label_exists(project):
    """Create a label for external contributors if it doesn't exist."""
    for label in project.labels.list(as_list=False):
        if label.attributes['name'] == EXTERNAL_LABEL_NAME:
            return

    # Label doesn't exist, create it.
    LOGGER.info('Creating new label for %s',
                project.attributes['path_with_namespace'])
    label = project.labels.create({'name': EXTERNAL_LABEL_NAME,
                                   'color': EXTERNAL_LABEL_COLOR,
                                   'description': EXTERNAL_LABEL_DESCRIPTION})


def get_vars_from_hook(var_list):
    """Get a dictionary of variables from pipeline webhook.

    The webhook data contains a list of dictionaries which is less fun to deal
    with than a regular key/value dictionary.
    """
    return {v['key']: v['value'] for v in var_list}


def handle_mr(gitlab_instance, project_config, mr_data):
    """Trigger pipelines for given MR."""
    # Get extra data
    extra_variables = {
        'git_url': mr_data['object_attributes']['source']['git_http_url'],
        'branch': mr_data['object_attributes']['source_branch'],
        'commit_hash': mr_data['object_attributes']['last_commit']['id'],
        'target_repo': mr_data['object_attributes']['target']['git_http_url'],
        'target_branch': mr_data['object_attributes']['target_branch'],
        'child_pipeline': 'True',
        'name': '{}-{}'.format(mr_data['project']['path_with_namespace'],
                               mr_data['object_attributes']['iid']),
        'title': '{}: MR {}'.format(mr_data['project']['path_with_namespace'],
                                    mr_data['object_attributes']['iid']),
        'mr_id': mr_data['object_attributes']['iid'],
        'origin_path': mr_data['project']['path_with_namespace']
    }

    project = gitlab_instance.projects.get(project_config['.mr_project'])
    merge_request = project.mergerequests.get(
        mr_data['object_attributes']['iid']
    )
    if not already_commented(merge_request, gitlab_instance.user.username):
        if cki_lib.misc.is_production():
            merge_request.discussions.create(
                {'body': WELCOME_MESSAGE.format(
                    group=project_config['.group']
                )}
            )

            # If this is the first time we see the MR that means there is no
            # label assigned to it yet.
            ensure_label_exists(project)
            merge_request.labels.append(EXTERNAL_LABEL_NAME)
            merge_request.save()
        else:
            LOGGER.info(
                'Not a production deployment, nod adding comments or label'
            )

    trigger = copy.deepcopy(project_config)
    trigger.update(extra_variables)
    trigger = utils.clean_config(trigger)

    status_message = 'Testing pipeline status:\n'

    # Trigger external pipeline if configured
    if '.cki_external_pipeline_branch' in project_config:
        trigger['cki_pipeline_branch'] = project_config[
            '.cki_external_pipeline_branch'
        ]
        trigger['cki_project'] = project_config[
            '.cki_external_pipeline_project'
        ]
        trigger['cki_pipeline_type'] = 'external'
        external_token = cki_lib.misc.get_env_var_or_raise(
            project_config['.external_trigger_token_name']
        )
        external_pipeline = utils.safe_trigger(
            gitlab_instance, external_token, [trigger]
        )[0]

        status_message = ''.join([
            status_message,
            'Basic',
            PIPELINE_MESSAGE.format(
                link=external_pipeline.attributes['web_url'],
                status='created',
                emoji=EMOJI_MAP['created'])
        ])

    # Trigger trusted pipeline if configured
    if '.cki_trusted_pipeline_branch' in project_config:
        trigger['cki_pipeline_branch'] = project_config[
            '.cki_trusted_pipeline_branch'
        ]
        trigger['cki_project'] = project_config[
            '.cki_trusted_pipeline_project'
        ]
        trigger['cki_pipeline_type'] = 'trusted'
        trigger['manual_trigger'] = 'True'
        trusted_token = cki_lib.misc.get_env_var_or_raise(
            project_config['.trusted_trigger_token_name']
        )
        trusted_pipeline = utils.safe_trigger(
            gitlab_instance, trusted_token, [trigger]
        )[0]

        status_message = ''.join([
            status_message,
            'Full',
            PIPELINE_MESSAGE.format(
                link=trusted_pipeline.attributes['web_url'],
                status='created',
                emoji=EMOJI_MAP['created']),
            '*If the full pipeline is not running, wait for a reviewer ',
            'to start it.*'
        ])

    if cki_lib.misc.is_production():
        merge_request.discussions.create({'body': status_message})
    else:
        LOGGER.info('Not a production deployment, not posting comments')
        LOGGER.info('STATUS COMMENT:\n%s', status_message)


# pylint: disable=too-many-locals
def update_pipeline_data(gitlab_instance, webhook_data):
    """Update existing MR with current pipeline status."""
    pipeline_id = str(webhook_data['object_attributes']['id'])
    pipeline_vars = get_vars_from_hook(
        webhook_data['object_attributes']['variables']
    )

    # Is this a pipeline we care about?
    project_path = pipeline_vars.get('origin_path')
    for project_config in CI_CONFIG.values():
        if project_path == project_config['.mr_project']:
            project = gitlab_instance.projects.get(project_path)
            break
    else:
        LOGGER.info('No matching MR project for %s, found %s',
                    webhook_data['commit']['url'],
                    project_path)
        return

    pipeline_status = webhook_data['object_attributes']['status']
    is_retriggered = cki_lib.misc.strtobool(pipeline_vars.get('retrigger',
                                                              'False'))
    if is_retriggered:
        LOGGER.info('Handling a retriggered pipeline, not posting comments')
        LOGGER.info('STATUS:\n%s', PIPELINE_MESSAGE.format(
            link='{}/pipelines/{}'.format(webhook_data['project']['web_url'],
                                          pipeline_id),
            status=pipeline_status,
            emoji=EMOJI_MAP[pipeline_status]
        ))
        return

    # Find the correct comment to edit. Bot only creates new threads so we
    # don't need to dig through every comment in discussions and it's enough to
    # check the top comments.
    mr_id = pipeline_vars['mr_id']
    merge_request = project.mergerequests.get(mr_id)
    bot_name = gitlab_instance.user.username
    for discussion in merge_request.discussions.list(as_list=False):
        first_comment_data = discussion.attributes['notes'][0]
        if first_comment_data['author']['username'] == bot_name and \
                pipeline_id in first_comment_data['body']:
            comment = discussion.notes.get(first_comment_data['id'])
            break
    else:
        # Did someone delete our original comment?
        LOGGER.error('Unable to find comment for %s in MR %s from %s',
                     webhook_data['commit']['url'],
                     mr_id,
                     project_path)
        return

    # Edit and update the comment
    body_lines = comment.body.splitlines()
    for index, line in enumerate(body_lines):
        if pipeline_id in line:
            new_line = line.rsplit('-', maxsplit=1)[0] + ' - {} {}'.format(
                pipeline_status, EMOJI_MAP[pipeline_status]
            )
            body_lines[index] = new_line
            break
    if cki_lib.misc.is_production():
        comment.body = '\n'.join(body_lines)
        comment.save()
    else:
        LOGGER.info('Not a production deployment, not posting comments')
        LOGGER.info('UPDATED COMMENT:\n%s', '\n'.join(body_lines))


def load_config(config_path):
    """Load webhook configuration.

    Read the config yaml, parse out the actual definitions and populate
    CI_CONFIG.
    """
    # Calm down pylint, there's no better way to pass along global settings if
    # process_message doesn't take extra args
    global CI_CONFIG  # pylint: disable=global-statement
    with open(config_path) as config_file:
        unparsed_config = load(config_file, Loader=Loader)

    CI_CONFIG = utils.process_config_tree(unparsed_config)


def get_project_config(project_name):
    """Get the configuration for given project."""
    for project_config in CI_CONFIG.values():
        if project_config['.mr_project'] == project_name:
            return project_config

    LOGGER.error('Missing config for %s', project_name)
    return {}


def get_manual_hook_data(gitlab_instance, mr_object, project_path):
    """Retrieve MR data that mimic the webhook so handler can be reused."""
    project = gitlab_instance.projects.get(project_path)
    # Source URL is not available in MR data so we have to do an extra project
    # query to get it
    source_project = gitlab_instance.projects.get(
        mr_object.attributes['source_project_id']
    )
    source_url = source_project.attributes['http_url_to_repo']

    return {
        'object_attributes': {
            'source': {'git_http_url': source_url},
            'source_branch': mr_object.attributes['source_branch'],
            'target': {'git_http_url': project.attributes['http_url_to_repo']},
            'target_branch': mr_object.attributes['target_branch'],
            'iid': mr_object.attributes['iid'],
            'last_commit': {'id': mr_object.attributes['sha']}
        },
        'project': {'name': project.attributes['name'],
                    'path_with_namespace': project_path},
        'user': {'username': mr_object.attributes['author']['username']}
    }


def process_mr_message(gitlab_instance, webhook_data):
    """Check everything about the MR message and call the handler."""
    project_name = webhook_data['project']['path_with_namespace']

    project_config = get_project_config(project_name)
    if not project_config:
        return

    author = webhook_data['user']['username']
    try:
        group = gitlab_instance.groups.get(project_config['.members_of'])
    except gitlab.GitlabGetError:
        group = gitlab_instance.projects.get(project_config['.members_of'])

    for member in group.members.all(as_list=False, all=True):
        if member.attributes['username'] == author:
            LOGGER.debug('Found internal contributor, not triggering')
            return

    # If we got this far we should trigger the pipeline(s)
    handle_mr(gitlab_instance, project_config, webhook_data)


# pylint: disable=unused-argument
def process_message(routing_key, payload):
    """Process the webhook message and call appropriate handlers."""
    message = Message(payload)
    webhook_data = message.payload

    object_kind = payload.get('object_kind')
    if not object_kind:
        LOGGER.error('Missing object_kind in message')
        return

    gitlab_instance = message.gl_instance()
    gitlab_instance.auth()

    # MR was submitted or modified
    if object_kind == 'merge_request':
        process_mr_message(gitlab_instance, webhook_data)

    # Pipeline for MR changed status
    elif object_kind == 'pipeline':
        update_pipeline_data(gitlab_instance, webhook_data)
    else:
        LOGGER.warning('Unknown hook type: %s', object_kind)


def main(args):
    """Set up and start consuming messages or handle manual requests."""
    load_config(cki_lib.misc.get_env_var_or_raise('CONFIG_PATH'))

    args = common.get_manual_args(args)
    if args.merge_request:
        gitlab_instance, mr_object, project_path = common.parse_mr_url(
            args.merge_request
        )
        gitlab_instance.auth()
        manual_hook_data = get_manual_hook_data(gitlab_instance,
                                                mr_object,
                                                project_path)
        process_mr_message(gitlab_instance, manual_hook_data)
        return

    settings.QUEUE.consume_messages(
        settings.WEBHOOKS_EXCHANGE,
        os.environ['EXTERNAL_CI_ROUTING_KEYS'].split(),
        process_message,
        queue_name=os.environ.get('EXTERNAL_CI_QUEUE')
    )


if __name__ == '__main__':
    main(sys.argv[1:])
