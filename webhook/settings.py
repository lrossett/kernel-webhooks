"""Initialize the message queue."""
import os

import sentry_sdk
from cki_lib import misc
from cki_lib.messagequeue import MessageQueue

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST', 'localhost')
RABBITMQ_PORT = int(os.environ.get('RABBITMQ_PORT', '5672'))
RABBITMQ_USER = os.environ.get('RABBITMQ_USER', 'guest')
RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD', 'guest')

WEBHOOKS_EXCHANGE = os.environ.get('WEBHOOK_RECEIVER_EXCHANGE', 'cki.webhooks')
BUGZILLA_QUEUE = 'bugzilla'

QUEUE = MessageQueue(RABBITMQ_HOST, RABBITMQ_PORT,
                     RABBITMQ_USER, RABBITMQ_PASSWORD)

if misc.is_production():
    sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'))
