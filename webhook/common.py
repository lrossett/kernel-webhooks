"""Common code that can be used by all webhooks."""
import argparse
import re
from urllib import parse

from cki_lib.gitlab import get_instance


def get_manual_args(args):
    """Intialize a parser for manual runs and return parsed arguments.

    Args:
        args: List of commandline arguments, excluding the program name.

    Returns: Parsed arguments
    """
    parser = argparse.ArgumentParser(
        description='Manual handling of merge requests')
    parser.add_argument('--merge-request',
                        help='Process given merge request URL only')
    parser.add_argument('--action', default='',
                        help='Action for the MR when using URL only')
    parser.add_argument('--oldrev', action='store_true',
                        help='Treat this as changed MR when using URL only')

    return parser.parse_args(args)


def parse_mr_url(url):
    """Parse the merge request URL used for manual handlers.

    Args:
        url: Full merge request URL.

    Returns:
        A tuple of (gitlab_instance, mr_object, project_path_with_namespace).
    """
    url_parts = parse.urlsplit(url)
    instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
    gl_instance = get_instance(instance_url)
    match = re.match(r'/(.*)/merge_requests/(\d+)', url_parts.path)
    project_path = re.sub('/-$', '', match[1])
    gl_project = gl_instance.projects.get(project_path)
    gl_mergerequest = gl_project.mergerequests.get(int(match[2]))

    return gl_instance, gl_mergerequest, project_path
